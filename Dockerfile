FROM filebrowser/filebrowser

RUN apk update && apk add git

RUN apk add build-base automake autoconf libxml2-dev fuse-dev curl-dev

RUN git clone https://github.com/s3fs-fuse/s3fs-fuse.git

RUN cd s3fs-fuse ; ./autogen.sh ; ./configure; make; make install

ENTRYPOINT []